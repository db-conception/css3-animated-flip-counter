var digitElList = document.getElementsByClassName("number");
var nbDigitCreated = digitElList.length;
function setValue(value){
  digitElList = document.getElementsByClassName("number");
  nbDigitCreated = digitElList.length;
  var nbDigitNewVal = value.toString().split("").length;
  console.log(nbDigitNewVal);
  // si pas assez de digit
  while(nbDigitNewVal > nbDigitCreated){
    prependDigitEl(digitElList[0].parentElement, 0);
    // on actualise le nb de digit
    nbDigitCreated++;
  }
  periodiq = setInterval(function(newValue = value) {
    var isDown = newValue < readCounterValue();
    if(readCounterValue() == newValue)
    {
      clearInterval(periodiq);
      return;
    }

    //on rajoute la class pour donner le sens de la modification
    var directionClassName = isDown ? "downFlip" : "upFlip";
    var digitWrappers = document.getElementsByClassName("number");
    for(var i = 0; i < digitWrappers.length; i++){
      digitWrappers[i].classList.add(directionClassName);
    }
    doFlip(nbDigitCreated-1 /*= les unités*/, isDown); 
  }, 1000);
}
setValue(10011);

function prependDigitEl(parent, defaultValue){
  var wrapper = document.createElement("span");
  wrapper.classList.add("number");
  wrapper.setAttribute("data-number", defaultValue);
  
  var primary = document.createElement("span");
  primary.classList.add("primary");
  
  var secondary = document.createElement("span");
  secondary.classList.add("secondary");
  
  wrapper.append(primary);
  wrapper.append(secondary);
  parent.prepend(wrapper);
}
//clearInterval(periodiq);
function readCounterValue()
{
  var digits = $(".number");
  var concatenedStr = "";
  for(var i = 0; i < digits.length; i++){
    concatenedStr += $(digits[i]).attr("data-number");
  }
  //console.log( Number(concatenedStr) );
  return Number(concatenedStr);
}
function doFlip(numberIndex, doDown = true) {
  
  var currentNumberElement = $(".number:eq(" + numberIndex + ")");
  
  var currentNumber = Number(currentNumberElement.attr("data-number"));    

  if(doDown === true)
  {
    currentNumber--;
    
    if (currentNumber < 0) {
      currentNumber = 9;

      if (numberIndex > 0) {
        doFlip(--numberIndex);
      }
    }
  }
  else
  {
    currentNumber++;
    
    if (currentNumber > 9) {
      currentNumber = 0;
      
      if (numberIndex < nbDigitCreated) {
        console.log(numberIndex -1);
        doFlip(--numberIndex, false);
      }
    }
  }
  
  
  currentNumberElement.addClass("flip");
  
  setTimeout(function() {
    currentNumberElement.attr("data-number", currentNumber);
    
    currentNumberElement.removeClass("flip");
  }, 500);
}