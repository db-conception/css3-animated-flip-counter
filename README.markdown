# CSS3 Animated Flip Counter
 _A Pen created at CodePen.io. Original URL: [https://codepen.io/db-conception/pen/RwPbprx](https://codepen.io/db-conception/pen/RwPbprx).

 It's simply a flip counter with CSS3 animation.. JS is only used to manage the count down.. it's light weight.. no images needed.. and doesn't use the background-position method.