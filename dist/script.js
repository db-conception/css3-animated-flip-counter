var currentValue = readCounterValue();
var doFlipDigit = function() {
  var newValue = 9980;
  if(readCounterValue() == newValue)
  {
    clearInterval(periodiq);
    return;
  }
  doFlip(3, true);
}
periodiq = setInterval(doFlipDigit, 1000);
//clearInterval(periodiq);
function readCounterValue()
{
  var digits = $(".number");
  var concatenedStr = "";
  for(var i = 0; i < digits.length; i++){
    concatenedStr += $(digits[i]).attr("data-number");
  }
  console.log( Number(concatenedStr) );
  return Number(concatenedStr);
}
function doFlip(numberIndex, doDown = true) {
  
  var currentNumberElement = $(".number:eq(" + numberIndex + ")");
  
  var currentNumber = Number(currentNumberElement.attr("data-number"));    

  if(doDown === true)
  {
    currentNumber--;
    
    if (currentNumber < 0) {
      currentNumber = 9;

      if (numberIndex > 0) {
        doFlip(--numberIndex);
      }
    }
  }
  else
  {
    currentNumber++;
    
    if (currentNumber > 9) {
      currentNumber = 0;

      if (numberIndex = 3) {
        doFlip(--numberIndex, false);
      }
    }
  }
  
  
  currentNumberElement.addClass("flip");
  
  setTimeout(function() {
    currentNumberElement.attr("data-number", currentNumber);
    
    currentNumberElement.removeClass("flip");
  }, 500);
}